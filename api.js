const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db/connect');
const res = require('express/lib/response');
const port = 8000;

let app = express();
app.use(bodyParser.json());

app.listen(port,()=>{
    console.log("Projeto executando");
});

app.get('/contatos', (req, res)=>{
    let cmd_selectAll = "SELECT * FROM CONTATO;";
    db.query(cmd_selectAll,(err, rows)=>{
        res.status(200).json(rows);
    });
});


app.get('/contatos/:id',(req, res)=>{
    let id = req.params.id;
    let cmd_selectId = "SELECT * FROM CONTATO WHERE ID = ?";
    db.query(cmd_selectId, id,(err, row)=>{
        res.status(200).json(row);
    });
});

app.post('/contatos', (req, res) =>{
    let dados = req.body;
    let cmd_insert = "INSERT INTO CONTATO (nome, idade, email) VALUES(?, ?, ?)";
    let dados_body = [dados.nome, dados.idade, dados.email];
    db.query(cmd_insert, dados_body,(error,row) =>{
        if(error){
            res.status(400).json({message:error});
        } else {
            res.status(201).json({message:"contato salvo com sucesso"})
        }
    })
});

app.delete('/contato/:id', (req, res) =>{
    let id = req.params.id;
    let cmd_delete = "delete from contato where id = ?";
    db.query(cmd_delete, id, (error, row) =>{
        if(error){
            res.status(400).send({message:error});
        } else { 
            res.status(200).json({message:"contato excluido com sucesso"})
        }
    })
});

app.put('/contato/:id', (req, res) =>{
    let dados = req.body;
    let id = req.params.id;
    let cmd_update = "update contato set nome = ?, idade = ?, email = ? where id = ?";
    let dados_body = [dados.nome, dados.idade, dados.email, dados.id];
    
    db.query(cmd_update, dados_body, (error, row) =>{
        if(error){
            res.status(400).send({message:error});
        } else { 
            res.status(200).json({message:"contato alterado com sucesso"})
        }
    })
});